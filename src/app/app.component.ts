import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import * as firebase from 'firebase'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  loadedFeature = 'recipe';

  onNavigate(feature: string) {
    this.loadedFeature = feature;
  }

  ngOnInit() {
    firebase.initializeApp({
      apiKey: "AIzaSyAZSWR028k6okTqJtPVxHU_iJe4QIPYRHQ",
      authDomain: "recipeproject-61ce4.firebaseapp.com"
    })
  }
}
