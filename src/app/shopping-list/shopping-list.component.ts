import { Component, OnInit } from '@angular/core';

import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from './shopping-list.service';
import { Subscription } from 'rxjs/Subscription';
import { OnDestroy } from '@angular/core';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit, OnDestroy {
  
  ingredients: Ingredient[]
  private subscription: Subscription

  constructor(private schoppingListservice: ShoppingListService) { }

  ngOnInit() {
    this.ingredients = this.schoppingListservice.getIngredients()
    this.subscription = this.schoppingListservice.ingredientsChanged.subscribe(
      (ingredients: Ingredient[]) => {
        this.ingredients = ingredients
      })
  }

  ngOnDestroy() {
    this.subscription.unsubscribe()
  }

  onEditItem(index: number) {
      this.schoppingListservice.startedEditting.next(index)
  }
}
